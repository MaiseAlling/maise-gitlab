class Flue {
    constructor() { // øversætter noget som classen kan forstå. 
        this.posX = random(width); // Gør så de bliver placeret random på skærmen
        this.posY = random(height);
        this.tid = frameCount;  // siger at fluens tid = frameCount
    }

    show() { // tegner fluen
        // tegner krop
        fill(150, 100, 50);
        ellipse(this.posX-30, this.posY-5, 50, 25);

        // tegner hoved
        fill(50, 100, 150);
        ellipse(this.posX-5, this.posY-5, 15, 15);

        // tegner øjne
        fill(255);
        ellipse(this.posX, this.posY-10, 5, 5);
        ellipse(this.posX, this.posY, 5, 5);

        // tegner vinger
        fill(255, 200);
        ellipse(this.posX-30, this.posY-15, 35, 15);
        ellipse(this.posX-30, this.posY+5, 35, 15);
    }
}


