//Mangler
//flue skal forsvinde når den er ramt
// point skal tælle op når flue er ramt
// game over når man har misset 7 fluer

//Laver globale variabler som jeg kan bruge senere i min kode

// laver et array, som skal bruges til for-loop. 
let flue = [];
let spiller;
let antal = 2;
let point = 0;
let lose = 0; 
let tid = 0;



// sætter baggrunden til at være dette billede. Samt preloader mine to giffer. 
function preload() {
    baggrund = loadImage("tabel.baggrund.avif");
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    spiller = new Spiller();

    //forloop som geenere så mange antal fluer, jeg ønsker og push 2 fluer ind i arrayet. Denne bliver sat i setup, 
    //da den kun skal køre 1 gang, elelrs ville den blive ved med at generere flere fluer. 
    for (let i = 0; i < antal; i++) {
        flue.push(new Flue());
    }
}

function draw() {
    // tegner baggrunden som jeg har preloadet. 
    background(baggrund);

    // her bruger jeg min variable, og ligger 1 til tid, efter hver frame der er 60 frames 
    //pr. sekund. Denne kan jeg bruge til mit lose-statement. 
    tid++

    // den tegner fluen på canvaset og kalder på funktionen
    showFlue();
    spiller.show();
    ramtFlue();
    pointDisplay();
   // checkResult();

   // laver et if-statement som siger, at hvis du ikke har fået 10 point inden for 1 minut, så vil du tabe
    if (tid == 1000) {
        fill(0);
        textSize(40);
        text("Too many flies in your food...GAME OVER", width/2, height/2);
  
      // Dette skal stoppe spillet og derfor ikke gå i loop. 
        noLoop();
      }
    


}

// laver en funktion som, som laver et for-loop, der tegner antallet af fluer. 
function showFlue() {

    // her bruger jeg arrayet, som jeg har sagt er 2, så viser altså de to objekter
    for (let i = 0; i < flue.length; i++) {

        // tegner selve fluen, og tager en ny flue fra arrayet og viser. 
        flue[i].show();

        /* laver et if-statement som sige, at hvsi framCounten er større
            end flue[1].tid +40, så skal den splice fluen og lave nye. vi har i vores flue Class, sat 
            this.tid, som er den vi referere til at være frameCount. [i], referere til vores let flue [], som er det
            antal fluer den skal tegner nemlig i alt 2, den referere altså til det for-loop, som vi har lavet oppe i 
            setup. + 80, vil sige at når fluen har været på skærmen i mere end frameCount + 40, så skal den forsvinde. 
            flue.splice (i, 1); er den der gør at fluen forsvinder, og flue.push(new Flue()); er den der gør at der bliver
            lavet nye fluer. */
        if (frameCount > flue[i].tid + 90) {
            flue.splice(i, 1);
            flue.push(new Flue());
        }
    }

}



function ramtFlue() {
    let distance; 
    for (let i = 0; i < flue.length; i++) {
        distance = dist(spiller.posX, spiller.posY, flue[i].posX, flue[i].posY); 

    if (distance < spiller.posX) {
       flue.splice(i,1); 
       flue.push(new Flue());
        point++; 
    }
    }
}


// laver en funktion hvor jeg tegner point, og placere den på canvas
function pointDisplay() {
    fill(0);
    textSize(40);
    text('Point;' + point, width / 2, 50);
   
}

//Laver en funktion til slut resultat, ved tab
/*function checkResult() {
    // laver et if-statemtn hvor jeg skriver at hvis lose, er større end point 
    //og hvis lose er større end 7, så skal der står følgende. 
    if (lose > point && lose > 7) {
      fill(0);
      textSize(40);
      text("Too many flies in your food...GAME OVER", width/2, height/2);

    // Dette skal stoppe spillet og derfor ikke gå i loop. 
      noLoop();
    }
}*/