# **MiniX3**

Her er et screenshot af mit program: 
<img src="Sky.png">

Her er et link til mit program: 
[https://maisealling.gitlab.io/maise-gitlab/Minix3/index.html]

Her er et link til min code: 
[https://gitlab.com/MaiseAlling/maise-gitlab/-/blob/main/Minix3/minix3.js]

## Hvad gør mit program ud på
Mit program er en throbber, som er en lysegrå sky på en grå baggrund. Skyen regner med vand og danner en vandpyt under sig. Dette fortsætter den med forevigt. 
Mit program er egentlig meget simpelt og måske lidt kedeligt, da dt er en grå sky på en grå baggrund. Den er ikke så interresant af kigge på, med en masse farver og underholdning. Jeg har valgt dette for at efspajle mine holdninger og følelser omkring throbbere. Dette vil jeg komme mere ind på i mine tanker bag min throbber. 

## Mine tanker bag min Throbber
Som sagt tidligere så er min throbber måske en anelse kedelig. Jeg har valgt at lave en grå og kedelig sky, på en grå og kedelig baggrund. Skyen regner med en masse regn. Jeg har valgt at lave denne lidt kedelige throbber, for at afspejle mine egne tanker og følelser omkring throbbere. Jeg finder det at vente på noget, meget kedeligt og irriterende. Tiden går, men det føles som om at tiden står stille og bare køre i loop, igen og igen. 
Personligt afhænger mit humør, meget af vejret på den gældene dag. Jeg blive selv sur og i dårligt humør, hvis det regner og er gråt en hel dag. Når dette sker, føler jeg at dagen er lang og kedelig og man ikke rigtig kan lave noget, men måske finder sig selv side inde på et værelse og se det næste afsnit af en serie, igen og igen og dagen bare går i loop. Det er lidt samme følelse jeg har prøvet at skabe med denne throbber. Jeg føler ikke en throbber skal være underholdene og spændene. Jeg føler istedet at en thobber afspejler at man venter på noget, som kan gøre at tiden går hurtigt. En throbber afpejler at tiden står stille og går i loop igen og igen. vandpytten underskyen, giver en følelse af at denne vente tid kan føles lang og at den langsomt vokser sig større og større, som afspejler at ens tålmodighed bliver fyldt mere og mere op og man blive mere og mere irriteret. 

## Hvad er real-tid? 
Tid. hvad er tid? Vi oplever tid meget forskelligt, selvom tiden altid er den samme. Nogle gange kan det føles som om at tiden går hurtigt, andre tidspunkter kan det føles som om at tiden går langsomt. 
Oplevelsen af tid er et væsentligt element i enhver form for erfaring eller kognition. Vores følelser om tidne går hurtigt eller langsomt, afhænger af de forventninger eller en begivenhed der kan være i fremtiden eller i nutiden. Det kan være noget vi glæder os til noget, og derfor føler at tiden går langsomt. Det kan være at vi laver noget der er virkelig sjovt, hvor vi glemmer tid og sted og derfor føles tiden hurtig. 

Menneskers tidsopfattelse var oprindeligt bundet i den vireklige verden, af naturens tid."Early observation was linked to observation of natural conditions like sunrise and sunset.” (Lammerant, p. 89) 
Mennesker har grundet globalisering og socialisering udviklet en tidserfaring, som også har påvirket teknologien. Maskiner, så som computere og telefoner har også dere naturlige cyklus. Dette er de vibrerende impulser i det indre ur. 
Mennesket har udviklet og implementeret nye tidsmæssige praksisser gennem cookies. På den måde er serveren i stend til at genkende brugere og deres tilstand i tid. 
Gennem teknologien kan vi skabe afstand til vores naturlige tid og designe vores egnetidsoplevelser. Vi kan bestemme, hvornår det skal være nat og hvornår det skal være nat. 

Så hvad er real-tid egentlig. Er klokken egentlig 18 når min computer siger den er 18, eller er det noget som mennesket blot har bestemt? Hvad er egentlig min naturlige tid? Er det når solen står op og går ned, eller er det hvad min computer eller telefon fortæller mig at den er? 
Computerens tidsopfattelse afhænger altså af den menneskelig tidsopfattelse. 

## Min kode
Jeg vil gerne have de objekter som jeg tegner, til at bevæge sig. Først har jeg tegnet objekterne, men dette bliver vidst senere. Her har jeg lavet mine globale variable, for at definere de objekter der skal bevæge sig, hvor og hvordan. Disse variabler skal kunne bruges hele koden igennem, og er derfor globale variabler. 
~~~~
// laver variable for speed
let speed = 1; 

// variable for y
let y = 750; 

// laver variable for pølens brede
let b = 50; 

// laver hastighed for pøl
let speedpool = 1; 


// Laver x placeringen til en variable, som vi kan bruge senere i koden. 
let regndropplaceringX; 

// laver y placeringen til en variable, og sætter den til 700, så kan vi bruge den senere i koden. 
let regndropplaceringY = 700; 
~~~~
~~~~
function setup() {
// sætter vinduet til vinduets brede og højde. 
createCanvas(windowWidth, windowHeight);
noStroke(); 
    
// definere regndråbens placering på X-aksen
regndropplaceringX = windowWidth/2; 
 
  }
~~~~
Her starter jeg min function draw. Jeg har her lavet et for-loop, så dråberne bliver ved med at falde fra toppen af skyen, ned til vandpytten, om og om igen. 
Får at gør at det er dråberne der gør det, har jeg lavet en funktion der hedder makedrop, som gør at det er dråberne der bliver tegnet. Jeg har tegnet selv funktionen længere ned i koden. Her bliver x og y placeringerne definere så dråberne falde random inde i dette interval. 
~~~~
function draw() {
// Sætter baggrunden til at være grå. Alle farveværdierne er det samme tal, dette skaber en grå tone. 
background(102, 102, 102); 

// laver et for-loop til regndråben så den kan bevæge sig.
//Fortæller den at når i = 0, og i er <= 100, så skal i plusse med 1, hver gang. Den tegner altså i alt 100 dråber. 
for(let i = 0; i <= 100; i++) {

// gør at dråberne falder random mellem værdierne -250 og 100 på x-aksen og mellem -100 og 100 på y-aksen. 
makedrop(regndropplaceringX + random(-250, 100), regndropplaceringY + random(-100, 100));   
      }
~~~~
Her tegner jeg skyen, ud af ellipser. 
~~~~
//* tegner sky
// sætter farvekoordinaterne for skyen. 
fill(204, 204, 204); 
noStroke(); 

// Tegner ellipserne som danne skyen. 
ellipse(windowWidth/2+70, windowHeight/2-200,300); // diameter her er 300
ellipse(windowWidth/2-70, windowHeight/2+1, 400); 
ellipse(windowWidth/2+140, windowHeight/2+1, 400);  
ellipse(windowWidth/2-300, windowHeight/2+1, 400); 
ellipse(windowWidth/2-200, windowHeight/2-250,350); 
~~~~
Her tegner jeg vandpytten (pøl), og laver et if-statement, som gør at vandpytten bevæger sig i en hastighed på 1. b står for breden på vandpytten. 
~~~~
 // tegner pøl og får pøl til at bevæge sig, med speeden 1. 
 // Når pølen er mindre eller = 600, så skal dan vokse med en peed på 1. 
if (b <= 600) {
speedpool = 1; 
 }
 // hvis pølen er > end 600, så skal pølens brede gå tilbage til at være 50 i breden. 
if(b > 600){ // breden af pølen stopper her. 
      background(102); 
      b = 50; // breden af pølen starter her. 
}
// tegner pølen, sætter farvekoordinaterne og tegner ellipsen. 
 fill(153, 255, 255); 
ellipse(windowWidth/2.1, windowHeight/2+400, b, 50); 

//tegner pøl og sætter b + b = speedpool som gør at pølen bevæger sig med den definerede speed. 
b = b + speedpool; 
~~~~
Her tegner jeg dråben og laver et if-statement og et else-statement. Dette definere hvor dråben skal starte og stoppe med at falde, og at placerungen skal vokse med 1 hver gang, som gør at den falder. 
~~~~
 //laver speed hastighed og placering 
//når y er mindre end 900, skal den vokse med 1, ellers så skal y gå tilbage til 700. 
  if(regndropplaceringY < 830) {
  regndropplaceringY++; 
  }
  else {
regndropplaceringY = 700; 
  }

// tegner dråben og laver den til en function som jeg kan bruge videre i min kode. 
function makedrop(x,y) {
  noStroke(); 
  fill(153, 255, 255); 
  ellipse(x, y, 20); 
  }
}
~~~~

## Hvad vil du udforske og/eller udtrykke?
Jeg vil udtrykke at det er kedeligt at vente, og at den realle tid derfor kan føles langsommere, end den måske egentligt er. Hvis jeg havde lavet en throbber, som var interresant at kigge på, havde tiden måske føltes hurtigere. 

## Hvordan kan vi karakterisere dette ikon anderledes?
Jeg synes dette er et svært spørgsmål. Jeg ved ikke, hvordan vil skal kunne karakterisere dette ikon anderledes. Jeg tror det kræver at vi får en anden ide om hvad tid er og hvordan tid er konstrueret. Throbber ikonet er karakteriserende for vente tid. Måske man vil kunne se det som tiden går, og hvis throbberen går hurtigt, så går tiden hurtigt. Dog fungere realtid ikke på denne måde, føler jeg. 
Jeg tror ikke jeg kan svare mere fyldestgørende på dette spørgsmål. 
