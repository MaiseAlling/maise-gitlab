## MiniX 7
Her er et link til mig kode: 
https://maisealling.gitlab.io/maise-gitlab/MiniX7/index.html

Her er nogle billeder af mit program: 
<img src="MiniX72.png">
<img src="MiniX7.png">

## Hvilken miniX har jeg valgt? 
Jeg har valgt at forbedre min miniX6 fra i sidste uge. 

## Hvilke ændringer har jeg lavet
Den fungerede ikke helt optimalt og det ville jeg godt have at den gjorde, samt har jeg også tilføjet nogle flere ting til den. I sidste uge fungerede min collision ikke, samt mine point. Dette har jeg fået til at virke denne gang, så man rent faktisk kan spille spillet. Jeg har også tilføjet en finalCount til spillet så man kan se, hvor mange fluer man har ramt og hvor mange fluer man har misset og derfor tabt spillet. 

## Min kode
Jeg vil ikke forklare min kode fra i sidste uge, den kan man gå ind og tjekke i min miniX6, men jeg vil gerne skrive om de ting jeg har tilføjet og ændret. 

I min ramt flue funktion som var den der ikke virkede i sidste uge, har jeg, lavet et for-loop og et if-statement. Funktionen sætter jeg op i draw()
~~~~
function ramtFlue() {
// laver her et for-loop, som siger at det der er inde i for-loopet skal gælde for alle objekter (flue) inde i arrayet. 
//for-loopet tæller fra 0 - 2 8med 2 tæller ikke med), og så bliver der hele tiden lagt en til. Første gange den looper, læser den som flue index 0 og anden gang læser den som flue index 1. Dette gør den da jeg har sat mit array for maks antal fluer til at være 2. i++ betyder at den skal ligger 1 til index hver gang. 
    for (let i = 0; i < flue.length; i++) {


//Hvis musens x position er større end fluens position - 10 (en afstand), så splice en flue med 1, og push en ny flue, og så point gå op. 
    if (mouseX > flue[i].posX - 10 && mouseX < flue[i].posX + 10) {
    flue.splice(i, 1);
    flue.push(new Flue());
    point++;
        }
    }
}
~~~~
Her laver jeg en funktion for min point. Skriver hvordan den skal se ud og hvor den skal være på canvas. 
~~~~
// laver en funktion hvor jeg tegner point, og placere den på canvas
function pointDisplay() {
    fill(0);
    textAlign(CENTER); 
    textSize(40);
    text('Point;' + point, width / 2, 50);

}
~~~~
Min final score funktion. 
~~~~
// Her laver jeg en funktion til min final score. Hvor jeg skriver at hvis du har tabt, så skriv dette. 
//Den laver (sværhedsgrad) til en variable i min funktion, som jeg bruger til at sige at når 10 gange sværhedsgrad 
//(bruger 10 fordi jeg øger min sværhedgrad med 10 hver gang), så skal den minusse point, så man kan se hvor mange man har tabt (hvor mange fluer man har misset). 
//final(tal) er argumentet, (sværhedgrad) er parameter. 
    function final(sværhedsgrad) {
    fill(0);
    textAlign(CENTER); 
    textSize(40);
//Her siger jeg at den skal skrive hvor mange fluer man har misset på dette sted på canvasset. 
    text('Flies you missed ' + (10*sværhedsgrad - point), width / 2, 100); 
}
~~~~
Oppe i min draw hvor jeg have et if-statement med tid før, har jeg lavet videre på dette, for at man kan se hvor mange point man får og hvor mange fluer man misser, samt hvor lang tid man har til at ramme dem. 
~~~~
 // laver et if-statement som siger, at hvis du ikke har fået 10 point inden for 5 millisekunder, så vil du tabe. Dette har jeg gjort flere gange for at man kan nå længere i spillet. 
    if (tid == 500 && point < 10) {
    fill(0);
    textAlign(CENTER); 
    textSize(50);
    text("Too many flies in your food...GAME OVER", width / 2, height / 2);
        
//Bruger min funktion og siger at den skal starte med den første sværhedsgrad, tallet er her argumentet. 
    final(1); 

// Dette skal stoppe spillet og derfor ikke gå i loop. 
    noLoop();
// laver et else if statement som siger at når der er gået 700 millisekunder og du ikke har ramt mindre end 20 fluer, så skriv dette, og brug parameteret for den næste final count. 
    } else if (tid == 700 && point < 20) {
    fill(0);
    textAlign(CENTER); 
    textSize(50);
    text("Too many flies in your food...GAME OVER", width / 2, height / 2);
    final(2); 
~~~~
## Hvad har jeg lært af denne MiniX? 
Først kunne jeg jo som sagt ikke få min collision til at virke, da jeg blev ved med at kigge på den fra ToFuGo spillet, og bruget spiller.posX og spiller.posY (det samme med flue), men da den ikke rigtig kunne forstå den her med .posX, brugte jeg i stedet mousX og mouse Y for mine mouse-koordinater. Det virkede her som om den skulle have en mere direkte placering og ikke rigtig forstod når jeg referedere til .posX og .posY i min spiller. I denne miniX fandt jeg også ud af at jeg kune bruge det her med parameter og argument, i en funktion, og på dne måe lave sværhedsgraden. Det synes jeg var ret fedt. Genrelt så har denne MiniX lært mig at dele mine ting lidt mere op. At vi har nogle objekter som vi laver og som vi så putter ind i vores program som ligesom observere de her objekter. Det gør det lidt mere overskueligt, når man lige har forstået det, at have flere js. filer som repræsentere noget forskelligt. 

## Hvordan kan jeg inkorporere eller videreudvikle koncepterne i min ReadMe/RunMe baseret på litteraturen vi har haft indtil nu. 
I denne miniX benyttede jeg også if- else- statements, for at lave mine sværhedsgrdaer og hvad der skulle ske hvis man tabte på de forskellige levels. Denne miniX kan måske også blive snakket om i forhold til det vi havde om emojis, og hvad der bliver vist eller ikke vist. Hvorfor har jeg lavte en lyshånd og ikke en blå?, hvorfor er det netop dette mad der bliver spist og ikke noget andet? osv. osv. Her kunne jeg lave nogle funktioner så man selv kunne designe sin hånd og hvilken mad man spiste. Man vil kunne gøre det ved at de skulle svare på nogle spørgsmål som så gjorde at der lev lavet et bord og en hånd der matchede deres svar. Ved at gøre dette vil man også kunne snakke om noget datafication og data-capturing. Spillerne ville give noget information om dem selv eller noget i de kan lide, som computeren så vil kunne bruge til at out-put til dem. 

## Hvad er relationen mellem æstetisk programmering og digital kultur? Hvordan demonstrere mit værk perspektiver i æstetisk programmering. 
Æstetisk programmering kan man sige er noget der prøver på at bygge oven på den digitale kultur og viderudvikle den (Soon & Cox). 

Vi betrakter programmering som en dynamisk kulturel praksis og fænomen, en måde at tænke og gøre i verden, hvordan vi forstår den og hvordan vi kan udspecificere nogle af de mere komplekse ting i vores verden. 
"We therefore consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities, in order to act upon those realities." (Soon & Cox, 2020, s. 14)

Æstetisk programmering udgør det mere kreative apsekt i denne sammenhænd. Den digitale kultur, er noget der hele tiden er omkring os i vores dagligdag. Vi bruger den selv, vi ser på den, vi udvikler den, vi snakker om den, vi kan lide den, vi hader den. Her er æstetisk programmering med til at tænke kreativt i i denne process, så vi kan forstå den og forholde os til den. Refelktere over den, og sætte spørgsmåls tegn ved den. Den æstetiske programmering behøver ikke nødvendigvis at være god for alle brugere for at komme med sit budskab, og designeren bag kan selv bestemme budskabet. 

Jeg synes der er nogle points i hovedbogen som beskriver æstetisk programmering til at være er forsøg på at udfordre eller bygge oven på den digitale kultur (Soon & Cox). Digital kultur er jo noget vi lever med i vores hverdag, hvor æstetisk programmering prøver at gøre noget creativt og anderledes ved denne digitale kultur. Så på samme side, så går de to betegelser hånd i hånd, da æstestetisk programmering tit er en udbygning eller forbedring af den digitale kultur der allerede er. Dog kan man måske også se æstetisk programmering som et kritisk modsvar til den digitale kultur vi allerede kender til.

I forhold til min egen miniX synes jeg at det giver god mening at vi tar noget vi ser ude i verden og programmere det, til i dette tilfælde noget sjovt som et spil, og udvikler det æstetisk, så vi forstår det og kan reflektere over det. På den måde bliver programmering mere knustnerisk og sjovt synes jeg. 
Spillet her tager også fat i hele underholdsnings ideen med vores digitale kultur. 





