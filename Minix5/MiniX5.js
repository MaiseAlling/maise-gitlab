
// Laver globale variabler
let distance; 
let size; 
let wait; 
let waitTime = 5000; 
let farve; 


function setup() {
    // Vi skal lave et canvas, der fylder hele skærmen
    createCanvas(windowWidth, windowHeight);
    //Sætter baggrundsfarven på canvaset
    background(255, 255, 255);
    frameRate(3);

}

function draw() {

//Sætter farven til random sort og hvid
    farve = random(255);

// sætter baggrunden til (255), så den hele tiden tegner oven på, samt alpha så den fader ud. 
    background(255,50);


//Ellipser

// Fill er farve
    fill(farve);

//Laver et for-loop, som gør at der kommer flere ellipse (50), i (index) sættes til 0, 
//da den starter der fra op til 50, i ++, gør at der bliver lagt 1 til 0 hele tiden. 
    for (let i=0; i < 50; i++){

//Denne gør at ellipsen bevæger sig rundt på canvaset random 
    x = random(windowWidth);
    y = random(windowHeight);

// laver distancen mellem cirklerne og firkanterne, så den er større i den ene ende af canvaset og mindre i den anden. I den lille ende er den distancen/2. 
distance = dist(x, y, 100, y); 
size = distance/20;
noStroke();

//Vi skal tegne en ellipse
    ellipse(x, y, size);
    }

 // starter i 0 og tæller der fra op til width, og laver en firkant hver 400 pixel af x-aksen
    for (let x = 0; x < width; x += 400) { 

// starter i 0 og tæller der fra op til height, og laver en firkant hver 100 pixel af x-aksen
        for (let y = 0; y <= height; y += 100) { 

 // tegner selve rectanglerne. 
            rect(x, y, 30, 30);
        }
    }


//Vente tid

// Laver et if-statement som siger at hvis farven er grå-ish, så vil den stoppe i ca. 5 sekunder
if(farve > 100) {
    // floor(millis()), betyder at wait skal være antallet af det millisekunder der gået siden programmet startede + vente tiden (5 sekunder)
wait = floor(millis()+ waitTime); 

// programmet stopper i 5 sekunder. 
while(millis() < wait) {}

}

}