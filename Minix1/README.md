# MiniX1

Her er et screenshot af mit program: 
<img src="Minix1/HusMiniX.jpg">


Her er et link til mit program: 
[https://MaiseAlling.gitlab.io/maise-gitlab/Minix1/index.html]

# Beskrivelse af programmet
Mit program er en børnetagning af et hus med en skorsten og en sol. Solen bevæger sig frem og tilbage på skærmen bag huset. Når solen når den ene ende af canvaset, bevæger den sig tilbage til den anden ende, så der opstår en form for ping pong effekt. Det samme sker med røgen, som bevæger sig op og ned bag skorstenen og op igen. 

# Forklaring af kode
Da jeg startede med at lave min kode, startede jeg med at tegne alle mine 2D figure da det gav mig en god ide af hvordan de forskellige figure kunne se ud. Her kunne jeg også lege med nogle farvekoordinater. Dette gav mig et godt fundament for programmet, og hvordan jeg skulle tilgå det. Efter det valgte jeg at prøve at få objekterne til at bevæge sig det var lidt mere svært. Men efter jeg fandt ud af, hvordan man gjorde så gav det god mening. Jeg fandt ud af hvordan man kan finde et givent punkt på skærmen, hvis man har et objekt der skal placeres et bestemt sted. Det var ret fedt. 

Først lavede jeg en Global variable der skulle styre min sol og min røg. 
En variable er en beholder for en værdi. Jeg navngiver variablerne. 

Jeg sætter min ellipses (røg) y koordinat til at være 46, da det er det den er i selve ellipsen. 

Jeg sætter den hastighed som røgen skal bevæge sig op ned, til at være 2. 

Jeg sætter min ellipses (sol) x koordinat til at være 100, da det er det den er i selve ellipsen. 

Jeg sætter den hastighed solen skal bevæge sig frem og tilbage til 4

Der skal forekomme to forskellige speed, så koden ved hvilken speed der bliver refereret til.
~~~~
//Global variable // Kaldte den Global variable
let y = 46; // Fastsætter y-koordinaten til 46. 
let speedrøg = 2; // Sætter hastigheden på røg til 2. 
let x = 100; // Fastsætter x-koordinaten til 100. 
speedsol = 4; // Sætter hastighed på sol til 4. 
~~~~
 
Her startede jeg mit canvas, og satte størrelse af den til automatisk at fylde hele siden ud. 
~~~~
function setup() { 
createCanvas(windowWidth, windowHeight); 
} // Fylder hele siden ud
~~~~
Nu startede jeg min fucktion draw(), som er det siden skal indeholde, altså farver, former, funktioner osv. 

Jeg beslutter at min baggrund skal være lyseblå, da jeg gerne vil lave noget der ligner en himmel. 
~~~~
function draw() {
// put drawing code here
  background(204, 255, 255); // Sætter farvekoordinaterne til lyseblå
~~~~
Nu har jeg lavet, og får den til at bevæge sig. Jeg har først kodet hvordan selve solen skal se ud, som ses nederst. Bevægelsen er kodet efter, selvom den står øverst. 

Solen bevægelse fra side til side er lavet ved brugen af et If else statement

Jeg har først fundet punktet hvor solen skal starte. Dette gjorde jeg ved at gå gøre min kode live --> dobbelt klikke på min mus --> vælge undersøg --> vælge console --> skrive mouseX og vælge denne --> placere min mus på det sted jeg vil have at min sol skal starte --> trykke enter på mit tastatur. 
Her fandt jeg frem til punktet 1, som er den ene ande af mit canvas. 

Jeg satte her if x <= 1) så starter solen her

Jeg brugte x-koordinaterne, da jeg ville have at min sol skulle rykke sig frem og tilbage på mit canvas. 

Herefter fandt jeg den anden ende af canvaset hvor solens andet ende punkt skulle være, dette gjorde jeg på samme måde som før.

Nu ville jeg fortælle min kode af den skulle bevæge sig frem og tilbage. Dette gjorde jeg ved at skrive min variable på solens hastighed, som vi difinerede til at starte med. 

Her skrev jeg gange -1 i begge ender, dette betyder minus gange minus giver plus, dette gør at når solen er i x-koordinatet 1 vil den pluse, pluse pluse og bevæge sig til den anden side, og dermed omvendt når solen er i x-koordinatet 1423. 

(Jeg er ikke helt sikekr på denne forklaring)

Neden under ses selve solen, hvordan den skal se ud. 

Her har jeg først skrevet hvilken farve solen skal være, for at cmputeren ved hvilket objekt der skal være denne farve, så den ikke farver alt i denne farve, skriver jeg fill og her efter farvekoderne i (). 

Herefter vælger jeg objektet circle skriver dets koordinater, for størrelse og placcering. Her ses at der kun står x. X-koordinatet definerede vi i starten og derfor skrives der kun x her. 

Herefter skriver jeg x = x + speedsol, som er med til at solen bevæger sig frem og tilbage. 
~~~~
//Sol bevæger sig
if(x <= 1) { // Sætter solens ene endepunkt
  speedsol = speedsol * -1;
} else if (x >= 1423) { // Sætter solens andet endepunkt
  speedsol = speedsol * -1;
}

//Sol
fill(255, 255, 0); // Angiver farvekoordinaterne for gul
circle(x, 200, 150); // Angiver cirklens koordinater
x = x + speedsol; // sætter solens retning og hastighed
~~~~
Herefter laver jeg huset
Dette gøres ved at vælge farverne og fill-functionen, samt objektet rektangel. 
~~~~
//Hus
  fill(255, 255, 0); // Angiver farvekoordinaterne for gul-ish
  rect(500, 450, 450, 900); // Angiver rektanglets koordinater, for størrelse og lokation
~~~~
Herefter laver jeg døren på samme måde som huset
~~~~
//Dør
  fill(0); // Angiver farvekoordinatet for sort
  rect(650, 600, 100, 1000); // Angiver rektanglets koordinater, for størrelse og lokation
~~~~
Herefter laver jeg håndtaget på samme måde som foregående
~~~~
//Håndtag
  fill(255, 255, 255); // Angiver farvekoordinatet for hvid
  circle(670, 700, 20); Angiver cirklens koordinater, for størrelse og lokation
~~~~
Nu laver jeg røgen. Dette gør jeg på samme måde som solen. Men benytter mig her af y-koodinaterne da røgen skal gå op og ned. 
~~~~
//Røg
fill(204, 204, 204);
ellipse(530, y, 20, 150);
y = y + speedrøg;

//Røg bevæger sig
if(y <= 15) {
  speedrøg = speedrøg * -1;
} else if (y >= 166) {
speedrøg = speedrøg * -1;
}
~~~~
Jeg laver nu de sidste objekter til at tegne mit hus
~~~~
//Skorsten
  fill(0);
  rect(500, 175, 75, 200);

//Tag
  fill(204, 51, 0);
  triangle(1050, 450, 400, 450, 725, 150);

//Vindue
fill(255, 204, 0);
rect(800, 500, 100, 100);
fill(0);
line(850, 600, 850, 500); // placere koordinaterne til linjen
line(900, 552, 800, 552); // placere koordinaterne til linjen
~~~~
Jeg vil gerne have at min tegning ligner en rigtig tegning, og vælger derfor at lave en kant rundt om alle mine objekter. 

Dette gør jeg ved at vælge funktionen StrokeCap()

Her vælger jeg koden strokeWeight(), og sætter tykkelsen af stregen til 5. 
Og koden strokeCap() og sætter stregen til at være rund
~~~~
//Kant om objekter
 strokeWeight(5); // Angiver tykkelsen af stregen 
strokeCap(ROUND); // Angiver stregens form. 

}
~~~~
# Min oplevelse
Jeg synes jeg havde en god oplevelse med det. Nogle af tingene var svære, som at få objekterne til at bevæge sig. Men folk var heldigvis gode til at hjælpe og dlee viden, så jeg følte at jeg hurtigt fandt ud af det. Jeg synes det sværeste var at finde rundt i GitLab og finde ud af hvordan jeg satte link, og billede ind i den. Der er mange ting lige at holde styr på. 

# Refleksioner omkring programmet
Programmet synes jeg fungere godt, det er rart med refferenecliste, så man nemt of hurtigt kan finde en kode og forstå den. Det gør at det er sjovt at bruge. 

Jeg synes personligt at det er sjovt at kode, men bliver nok hurtigt frustreret, hvis ikke det fungere for mig. Der synes jeg at P5JS fungere virkelig godt til at hjælpe en. 

Det var alt i alt en god oplevelse, med min første MiniX. 

# Opgraderingsmuligheder
Hvis jeg skulle forbedre min kode, kunne det være fed at få solen til at gå over huset i en bue og forsvinde væk fra skærmen. Hvor der herefter ville komme en måne op og baggrunden ville skifte til mørkeblå. 

Jeg valgte at holde mig til det jeg følte jeg havde lidt styr på og det jeg tænkte var nogenlunde overskueligt og nemt at tage hånd om, få at jeg havde en god første oplevelse. Når jeg har fået mere styr på programmet, kan jeg gå mere på opdagelse og måske udfordre mig selv lidt mere. 
