//Global variable
let y = 46;
let speedrøg = 2;
let x = 100; 
speedsol = 4; 

function setup() {
// put setup code here
createCanvas(windowWidth, windowHeight); 
}

function draw() {
// put drawing code here
  background(204, 255, 255);

//Sol bevæger sig
if(x <= 1) {
  speedsol = speedsol * -1;
} else if (x >= 1423) {
  speedsol = speedsol * -1;
}

//Sol
fill(255, 255, 0);
circle(x, 200, 150);
x = x + speedsol; 

//Hus
  fill(255, 255, 0);
  rect(500, 450, 450, 900);

//Dør
  fill(0);
  rect(650, 600, 100, 1000);

//Håndtag
  fill(255, 255, 255);
  circle(670, 700, 20);

//Røg
fill(204, 204, 204);
ellipse(530, y, 20, 150);
y = y + speedrøg;

//Røg bevæger sig
if(y <= 15) {
  speedrøg = speedrøg * -1;
} else if (y >= 166) {
speedrøg = speedrøg * -1;
}

//Skorsten
  fill(0);
  rect(500, 175, 75, 200);

//Tag
  fill(204, 51, 0);
  triangle(1050, 450, 400, 450, 725, 150);

//Vindue
fill(255, 204, 0);
rect(800, 500, 100, 100);
fill(0);
line(850, 600, 850, 500);
line(900, 552, 800, 552);

//Kant om hus
 strokeWeight(5);
strokeCap(ROUND);

}
