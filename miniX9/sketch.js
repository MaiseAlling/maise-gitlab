let myJson;

//afstanden mellem hver individuel linje i teksten
let meningsdannelse = 30

//forskyder hele teksten langs x-aksen
let sandheden = 60 

//forskyder hele teksten langs y-aksen
let systematiskeLøgne = 160  

//preloader vores 5 billeder samt Json fil
function preload() {
    myJson = loadJSON("jam.json");
    img1 = loadImage('eb.png');
    img2 = loadImage('fk.png');
    img3 = loadImage('p.png');
    img4 = loadImage('pf.png');
    img5 = loadImage('pt.png');
}

//laver canvas og tekstformatering samt framerate
function setup() {
    createCanvas(windowWidth, windowHeight);
    textFont('Geneva')
    fill(75);
    textSize(16);
    frameRate(0.3)
}

function draw() {
    //ændrer billedestørrelsen så den fylder hele canvas
    img1.resize(width, height)
    img2.resize(width, height)
    img3.resize(width, height)
    img4.resize(width, height)
    img5.resize(width, height)
    //random bliver kun hele tal pga. ceiling
    
    let r = ceil(random(5))
    //r gør at vi skifter baggrundsbillede tilfældigt sammen med nedenstuende if-else statements
    if (r == 1) {
        image(img1, 0, 0);
    } if (r == 2) {
        image(img2, 0, 0);
    } if (r == 3) {
        image(img3, 0, 0);
    } if (r == 4) {
        image(img4, 0, 0);
    } if (r == 5) {
        image(img5, 0, 0);
    }
    // Her tilgår vi Json filen og indsætter teksten. floor(random) bestemmer hvad der skal stå på hver linje
    text(myJson.intro[floor(random(5))], width / 2 - sandheden, height / 2 + systematiskeLøgne);
    text(myJson.kryds[floor(random(9))], width / 2 - sandheden, height / 2 + systematiskeLøgne + meningsdannelse);
    text(myJson.cirkel[floor(random(30))], width / 2 - sandheden, height / 2 + meningsdannelse * 2 + systematiskeLøgne);
    text(myJson.afslutning[floor(random(12))], width / 2 - sandheden, height / 2 + meningsdannelse * 3 + systematiskeLøgne);
    console.log(r)
}